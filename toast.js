let _toastsContainer = {};

// eslint-disable-next-line no-unused-vars
function toast(
  message,
  {
    type = "info",
    timeout = 3000,
    animationDuration = 200,
    animationDurationOpen = animationDuration,
    animationDurationClose = animationDuration,
    closeOnClick = true,
    className,
    position = "bottom-right",
    borderRadius = 20,
    keepOpenIfHovered = true,
    text = false,
  } = {}
) {
  if (timeout < 500 && timeout !== 0 && timeout) {
    throw Error("timeouts under 500 are not allowed");
  }
  if (
    (animationDurationOpen < 50 &&
      animationDurationOpen !== 0 &&
      animationDurationOpen) ||
    (animationDurationClose < 50 &&
      animationDurationClose !== 0 &&
      animationDurationClose)
  ) {
    throw Error("animation durations under 50 are not allowed");
  }
  if (timeout && timeout < animationDurationOpen + animationDurationClose) {
    throw Error(
      "the timeout must be greater than two times the animation duration"
    );
  }

  if (!_toastsContainer[position]) {
    // check if position is correct
    const positions = position.split("-");
    if (
      (positions.length === 1 &&
        !["bottom", "top", "left", "right", "center"].includes(position)) ||
      (positions.length === 2 &&
        ((["bottom", "top"].includes(positions[0]) &&
          !["left", "right"].includes(positions[1])) ||
          (["left", "right"].includes(positions[0]) &&
            !["bottom", "top"].includes(positions[1])))) ||
      (positions.length !== 1 && positions.length !== 2)
    ) {
      throw Error("unsupported position");
    }

    // init toasts container
    _toastsContainer[position] = document.createElement("div");
    _toastsContainer[position].style["position"] = "fixed";
    _toastsContainer[position].style[positions[0]] = "10px";
    _toastsContainer[position].style["display"] = "flex";
    _toastsContainer[position].style["flex-direction"] = "column";
    if (positions.length > 1) {
      _toastsContainer[position].style[positions[1]] = "10px";
      _toastsContainer[position].style["align-items"] = position.includes(
        "left"
      )
        ? "flex-start"
        : "flex-end";
      // also set position where the container shouldn't be aligned to prevent to small edges on mobile
      if (position.includes("left")) {
        _toastsContainer[position].style["right"] = "10px";
      } else {
        _toastsContainer[position].style["left"] = "10px";
      }
    } else {
      if (["bottom", "top", "center"].includes(position)) {
        _toastsContainer[position].style["align-items"] = "center";
        _toastsContainer[position].style["left"] = "50%";
        _toastsContainer[position].style["transform"] = "translate(-50%)";
        if (position === "center") {
          _toastsContainer[position].style["top"] = "50%";
        }
      } else {
        _toastsContainer[position].style["top"] = "50%";
        _toastsContainer[position].style["align-items"] =
          position === "left" ? "flex-start" : "flex-end";
      }
    }
    document.body.appendChild(_toastsContainer[position]);
  }

  let hideTimeout = null;
  let hovered = false;

  const toastTypes = {
    info: "black",
    success: "green",
    warning: "orange",
    error: "red",
    danger: "red",
  };

  const toast = document.createElement("div");
  toast.style["background-color"] = toastTypes[type] ? toastTypes[type] : type;
  toast.style["color"] = "white";
  toast.style["border-radius"] = borderRadius + "px";
  toast.style["padding"] = "1rem 2rem";
  toast.style["margin"] = "0.5rem";
  toast.style["opacity"] = (animationDurationOpen ? 0 : 1).toString();
  if (keepOpenIfHovered && timeout) {
    toast.addEventListener("mouseenter", () => {
      hovered = true;
    });
    toast.addEventListener("mousemove", () => {
      hovered = true;
    });
    toast.addEventListener("mouseleave", () => {
      hovered = false;
    });
  }
  toast.className = className;
  if (closeOnClick) {
    toast.addEventListener("click", () => {
      if (hideTimeout) {
        clearTimeout(hideTimeout);
      }
      toast.remove();
    });
  }
  if (text) {
    toast.innerText = message;
  } else {
    toast.innerHTML = message;
  }
  _toastsContainer[position].appendChild(toast);

  if (animationDurationOpen) {
    const animationSteps = animationDurationOpen / 10;
    let stepsDone = 0;
    let timer = setInterval(() => {
      if (stepsDone >= animationSteps) {
        clearInterval(timer);
        return;
      }
      stepsDone++;
      toast.style["opacity"] = (stepsDone / animationSteps).toString();
    }, 10);
  }

  if (timeout) {
    hideTimeout = setTimeout(
      () => {
        const hide = () => {
          if (animationDurationClose) {
            const animationSteps = animationDurationClose / 10;
            let stepsDone = 0;
            let timer = setInterval(() => {
              if (stepsDone >= animationSteps) {
                toast.remove();
                clearInterval(timer);
                return;
              }
              stepsDone++;
              toast.style["opacity"] = (
                1 -
                stepsDone / animationSteps
              ).toString();
            }, 10);
          } else {
            toast.remove();
          }
        };
        if (hovered) {
          toast.addEventListener("mouseleave", hide);
        } else {
          hide();
        }
      },
      animationDurationClose ? timeout - animationDurationClose : timeout
    );
  }
}
