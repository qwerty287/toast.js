# toast.js

> Easily send toasts in a browser

toast.js provides a toast functionality in browsers to offer nice feedback to the user.

## Features

- fully JavaScript-based and only one file
- [very easy to use](#usage)
- very light-weight, no dependencies
- supports `info`, `success`, `warning`, `error` and `danger` types
- fade in and out
- [many configuration options](#options)

## Usage

First, you need the toast.js file.

To import from source, use this snippet:

```html
<script src="https://qwerty287.codeberg.page/toast.js/200.js"></script>
```

If you want to serve it locally, just download the `toast.js` file and import it in your HTML.

Then, you can create toasts:

```javascript
toast("Hello World!");
```

The `toast` function accepts two arguments: the message, and an optional object to change options.

### Options

Options are managed using a simple object as second parameter.

| Option                   | Explanation                                                                                                      | Type                                                                                                                                                                                                                             | Default             |
| ------------------------ | ---------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------- |
| `type`                   | Changes the background color of the toast                                                                        | string, either a CSS color or `info` (black background), `success` (green background), `warning` (orange background), `error` (red background) or `danger` (red background)                                                      | `info`              |
| `timeout`                | How long the toast is visible, minimum value 500, 0 or `null` disables timeout (toasts won't disappear)          | integer in milliseconds                                                                                                                                                                                                          | 3000                |
| `animationDuration`      | Time for the fade in and out animation, minimum value 50, 0 or `null` disables animation                         | integer in milliseconds                                                                                                                                                                                                          | 200                 |
| `animationDurationOpen`  | Time for the fade in animation, minimum value 50, 0 or `null` disables animation, overrides `animationDuration`  | integer in milliseconds                                                                                                                                                                                                          | `animationDuration` |
| `animationDurationClose` | Time for the fade out animation, minimum value 50, 0 or `null` disables animation, overrides `animationDuration` | integer in milliseconds                                                                                                                                                                                                          | `animationDuration` |
| `closeOnClick`           | Whether to close the toast if the user clicks on the toast                                                       | boolean                                                                                                                                                                                                                          | true                |
| `className`              | Name of custom CSS class that will be added to the toast (maybe you have to add `!important` to your rules)      | string, a CSS class                                                                                                                                                                                                              | none                |
| `position`               | Where the toast will appear                                                                                      | string, vertical positions: `top`, `bottom`, horizontal positions: `left`, `right`, use one of them or a connection of a vertical and a horizontal one (with a hyphen between then) or use `center` to center in both directions | `bottom-right`      |
| `borderRadius`           | Change the border radius of the toasts                                                                           | integer in CSS `px` unit (e.g. 10 means `10px` in CSS)                                                                                                                                                                           | 20                  |
| `keepOpenIfHovered`      | Do not close the toast if it is hovered                                                                          | boolean                                                                                                                                                                                                                          | `true`              |
| `text`                   | Do not render the content as HTML                                                                                | boolean                                                                                                                                                                                                                          | `false`             |

Some examples:

```javascript
toast("toast is visible for 10 s with red brackground", {
  type: "error",
  timeout: 10000,
});
```

```javascript
toast("toast is visible for 3 s with blue brackground", { type: "blue" });
```

For more examples, check out the [examples](https://qwerty287.codeberg.page/toast.js).
